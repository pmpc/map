# FOSS friendliness map

![Map screenshot](screenshot/screenshot.png)

This map displays Free Software use in the public administrations, as well as how
friendly the EU countries are toward Free Software, in term of legislation and
policies.

## Features

* Multi-layer map. We have data about countries and public administrations. The
  public administrations data are only shown when zooming on a country
* Country selection. Give the ability to quickly jump to a given country
* Display a pop-up by clicking on an object. We can display detailed
  description or graphics in a pop-up

## How to deploy this?

As everything is rendered client-side, the map can be hosted by any web server.

## The data

For now we use dummy data. **The data displayed on the map is currently
randomly generated**.

### Files

There are three data files:

1. [countries.js](data/countries.js). See this [script](dummy-data.pl) for details.
2. [geo-cities.json.js](data/geo-cities.json.js) is a GeoJSON file used to put cities
   on the map.
3. [geo-countries.json.js](data/geo-countries.json.js) is a GeoJSON file used to put
   countries on the map.

### Format

For the data format, we use the [GeoJSON
standard](https://en.wikipedia.org/wiki/GeoJSON). You can see an example on the
Wikipedia page. Here is a sample of the data used for this map:

```
var cities = {
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": { "type": "Point", "coordinates": [2.3522219000000177, 48.856614 ] },
            "properties": {
                "name": "Paris",
                "text": "BlaBla"
            }
        },
        {
            "type": "Feature",
            "geometry": { "type": "Point", "coordinates": [2.1734034999999494, 41.38506389999999]},
            "properties": {
                "name": "Barcelona",
                "text": "BlaBla"
            }
        }
    ]
}
```

Under the `properties` object we can put arbitrary data for each city and for
each country, this can be used to display information.

## Technology stack

This project relies on [leaflet](http://leafletjs.com/) to draw the map.

## License

This software is copyright 2018 by the Free Software Foundation Europe e.V. and
licensed under the GPLv3 license. For details see the "LICENSE" file in the top
level directory of https://git.fsfe.org/pmpc/map.

