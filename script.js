let mymap = L.map('map').fitBounds([[64.12652059999999, -21.817439199999967], [38.969719, 59.55627800000002]]);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    opacity: 1,
    maxZoom: 9,
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="http://mapbox.com">Mapbox</a>',
    id: 'mapbox.streets'
}).addTo(mymap);

// Associate ranks with colors and with the legend's text
const ranks = {
    1: {
        hue: 120,
        text: 'Friendly'
    },
    2: {
        hue: 38,
        text: 'In progress'
    },
    3: {
        hue: 0,
        text: 'Unfriendly'
    }
};

// Associate each layer polygons with the geojson ID
let counter = 0;
let polygons = {};

// Create the polygons for the countries
countries_layer = L.geoJSON(countries, {
    style: function (test){
        rank = data_country[test.properties.name];
        return {
            weight: 2,
            color: '#999',
            opacity: 1,
            fillColor: getColorCountry(rank),
            fillOpacity: 0.8
        };
    },
    onEachFeature: onEachFeature
}).addTo(mymap);

// Create the marker for the cities
cities_layer = L.geoJSON(cities, {
    pointToLayer: function(feature, latlng) {
        return new L.CircleMarker(latlng, {
            radius: 5,
            fillOpacity: 0.75,
            color: 'red'
        });
    },
    onEachFeature: onEachCity,

});

// If we zoom in, display the cities layer
mymap.on('zoomend', function() {
    if (mymap.getZoom() > 6){
        if (!mymap.hasLayer(cities_layer)) {
            mymap.addLayer(cities_layer);
        }
    }
    if (mymap.getZoom() <= 6){
        if (mymap.hasLayer(cities_layer)){
            mymap.removeLayer(cities_layer);
        }
    }
});

let legend = L.control({position: 'topleft'});

legend.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend');
    div.innerHTML = '<p style="margin-top:15%"><strong>Free Software friendliness</strong></p>';
    // for (var i = 0; i < grades.length; i++) {
    for (rank in ranks) {
        div.innerHTML +=
            '<i style="background: hsl(' + ranks[rank].hue + ', 100% , 70%)"></i><strong>' + ranks[rank].text + '</strong></br>';
    }

    return div;
};

legend.addTo(mymap);

countries_layer.idHighlighted = -1;

function getColorCountry(rank) {
    return  'hsl(' + ranks[rank].hue + ', 100%, 85%)';
}

// Handle mouseover events for countries
function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

}

// Select the country user clicks on a country in the dropdown menu
function highlightFeatureLayer(layer) {
    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

}

function onEachFeature(feature, layer) {

    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight
    });

    const name = feature.properties.name;

    // Popup when the user clicks on a country
    var div = $('<div class="popupGraph" id="' + name + '" style="display:block;width: 150; height:150;">' + name +
                '</div>')[0];
    let popup = L.popup().setContent(div);
    layer.bindPopup(popup);

    polygons[counter] = layer;
    ++counter;
}

function onEachCity(feature, layer) {

    const name = feature.properties.name;
    const text = feature.properties.text;

    // Popup when the user clicks on a city
    var div = $('<div class="popupGraph" id="' + name + '" style="display:block;width: 150; height:150;">' + name + '</br>' + text +
                '</div>')[0];
    let popup = L.popup().setContent(div);
    layer.bindPopup(popup);
}


function pointToMap(id) {
    if (countries_layer.idHighlighted != -1) {
        countries_layer.resetStyle(polygons[countries_layer.idHighlighted]);
    }
    countries_layer.idHighlighted = id;
    layer = polygons[id];
    highlightFeatureLayer(layer);
    mymap.panTo(L.geoJSON(countries.features[id]).getBounds().getCenter());
    zoomToFeature(id);
}


function zoomToFeature(i) {
    mymap.fitBounds(polygons[i].getBounds());
}

function resetHighlight(e) {
    countries_layer.resetStyle(e.target);
}
