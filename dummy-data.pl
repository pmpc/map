use JSON;
use strict;
use warnings;

open my $fh, '<', 'data/geo-countries.json' or die $!;
$/ = undef;
my $data = <$fh>;
close $fh;

my $json = decode_json ($data);

my %output;

# For every country generate a random number between 1 and 3
for my $country(@{$json->{features}}) {
    $output{$country->{properties}->{name}} = 1 + int(rand(3));
}

print to_json \%output;
